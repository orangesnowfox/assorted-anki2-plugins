# Copyright Zachery Gyurkovitz 2018 MIT License -- see LICENSE.md or https://opensource.org/licenses/MIT

from aqt import mw
from aqt.utils import showInfo
from aqt.qt import *

import time
from datetime import datetime

class NextDue:
    def __init__(self, mw):
        # set shortcut here (yes this comment is useless, but hopefully it makes the following line stick out)
        self.shortcut = "ALT + N"
        
        self.menuAction = QAction("Next Due Card (%s)" % self.shortcut, mw)
        mw.connect(self.menuAction, SIGNAL("triggered()"), self.next_due)
        mw.form.menuTools.addAction(self.menuAction)
        
        self.shortcut = QShortcut(QKeySequence(self.shortcut.replace(" ", "")), mw)
        self.shortcut.connect(self.shortcut, SIGNAL("activated()"), self.next_due)

    def get_due_timestamp(self, now):
        duetimestamp = mw.col.db.scalar("select min(due) from cards where queue is 1 and due > ?", now)
        
        if duetimestamp is None:
            duetimestamp = mw.col.db.scalar("select min(due) from cards where queue is 1")
        return duetimestamp
    
    
    def calculate_due(self):
        now = time.time()
        duetimestamp = self.get_due_timestamp(now)
        
        if duetimestamp is None:
            return "No more cards in the learning queue!"
        
        duetime = datetime.fromtimestamp(duetimestamp)
        timetilldue = duetime - datetime.fromtimestamp(now)
        
        if timetilldue.total_seconds() < 0:
            return "All cards are already due!"
        else:
            dueAtStr = duetime.strftime("%H:%M")
            hours = timetilldue.seconds // 3600
            minutes = timetilldue.seconds // 60 % 60
            return "Next card due at: %s\nDue in: %02d:%02d" %(dueAtStr, hours, minutes)
        
    def next_due(self):
        showInfo(self.calculate_due())
        
if __name__ != "__main__":
    # Save a reference to the toolkit onto the mw, preventing garbage collection of PyQT objects
    if mw: mw.nextdue = NextDue(mw)
else:
    print "This is a plugin for the Anki Spaced Repition learning system and cannot be run directly."
    print "Please download Anki2 from <http://ankisrs.net/>"
